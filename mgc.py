"""
MGC Application
---------------

This is the desktop version of MonGlaucome
"""
from typing import (
    List,
    Tuple,
)

import datetime as dt

from mgcdata import (
    PIO,
    Examen,
    Medicament,
    Traitement,
    TypeGlaucome,
    Patient,
)

from itertools import groupby

from statistics import mean

import sys

from PySide2.QtCharts import QtCharts

from PySide2.QtCore import (
    QPoint,
    QPointF,
    QSize,
    QTime,
    Qt,
)

from PySide2.QtGui import (
    QKeySequence,
    QPainter,
)

from PySide2.QtPrintSupport import (
    QPrintDialog, )

from PySide2.QtWidgets import (
    QAction,
    QApplication,
    QCalendarWidget,
    QDialog,
    QDoubleSpinBox,
    QDialogButtonBox,
    QFileDialog,
    QFormLayout,
    QFrame,
    QGroupBox,
    QHBoxLayout,
    QLabel,
    QLineEdit,
    QListWidget,
    QListWidgetItem,
    QMainWindow,
    QMenuBar,
    QMessageBox,
    QRadioButton,
    QScrollArea,
    QSpinBox,
    QTableWidget,
    QTextEdit,
    QTimeEdit,
    QTableWidgetItem,
    QTabWidget,
    QVBoxLayout,
    QWidget,
)

from mgcserdes import (
    save_patient_file,
    load_patient_file,
    save_database_file,
    load_database_file,
)


def to_scatter_points(
        examens: List[Examen]) -> Tuple[List[QPointF], List[QPointF]]:
    droite = []
    gauche = []
    for e in examens:
        droite.append(QPointF(e.moment.hour, e.pio.droite))
        gauche.append(QPointF(e.moment.hour, e.pio.gauche))
    return (droite, gauche)


def to_spline_points(examens: List[Examen], reduction: float = 0
                     ) -> Tuple[List[QPointF], List[QPointF]]:
    sorted_exams = sorted(examens, key=lambda e: e.moment.hour)
    droite = sorted([
        QPointF(heure,
                float(mean([e.pio.droite for e in exs])) * (1 - reduction))
        for (heure, exs) in groupby(sorted_exams, lambda e: e.moment.hour)
    ],
                    key=lambda point: point.x())
    gauche = sorted([
        QPointF(heure,
                float(mean([e.pio.gauche for e in exs])) * (1 - reduction))
        for (heure, exs) in groupby(sorted_exams, lambda e: e.moment.hour)
    ],
                    key=lambda point: point.x())
    return (droite, gauche)


def amplitude(l):
    return max(l) - min(l)

def to_fluctuation_points(examens: List[Examen]) -> Tuple[List[QPointF], List[QPointF]]:
    sorted_exams = sorted(examens, key=lambda e: e.moment.hour)
    droite = sorted([
        QPointF(heure, amplitude([e.pio.droite for e in exs]))
        for (heure, exs) in groupby(sorted_exams, lambda e: e.moment.hour)
    ],
                    key=lambda point: point.x())
    gauche = sorted([
        QPointF(heure, amplitude([e.pio.gauche for e in exs]))
        for (heure, exs) in groupby(sorted_exams, lambda e: e.moment.hour)
    ],
                    key=lambda point: point.x())
    return (droite, gauche)

class PIOStats():
    '''Statistiques tirées d'une liste de mesures de PIO. Cette classe contient
    les membres suivants:

    - pic_[droite|gauche]: indique la PIO maximum mesurée pour chaque oeil. Si
      aucune mesure n'a été faite, ces variables sont `None`.

    - creux_[droite|gauche]: équivalents de `pic_[droite|gauche]` pour le
      minimum mesuré.

    - moyenne_[droite|gauche]: équivalents de `pic_[droite|gauche]` pour la
      moyenne mensurée.

    - fluctuations_lt_[droite|gauche]: représente le maximum de variation
      mesurée pour chaque oeil au cours d'examens effectués à la même heure. Si
      tous les examens ont étés faits à des heures différentes, ces variables
      sont `None`.

    - fluctuations_ct_[droite|gauche]: représente l'amplitude des mesures pour
      chaque oeil indépendamment de l'heure de mesure. Seulement les derniers X
      jours d'examens effectuées sont pris en compte (habituellement 31 jours).

    '''

    def __init__(self,
                 examens: List[Examen],
                 ct_limit: dt.timedelta = dt.timedelta(days=31)):
        examens = sorted(examens, key=lambda e: e.moment.hour)
        if examens:
            self.pic_droite = max([e.pio.droite for e in examens])
            self.pic_gauche = max([e.pio.gauche for e in examens])
            self.creux_droite = min([e.pio.droite for e in examens])
            self.creux_gauche = min([e.pio.gauche for e in examens])
            self.moyenne_droite = sum([e.pio.droite
                                       for e in examens]) / len(examens)
            self.moyenne_gauche = sum([e.pio.gauche
                                       for e in examens]) / len(examens)
        else:
            self.pic_droite = None
            self.pic_gauche = None
            self.creux_droite = None
            self.creux_gauche = None
            self.moyenne_droite = None
            self.moyenne_gauche = None

        grouped = {
            heure: [e for e in examen]
            for (heure, examen) in groupby(examens, lambda e: e.moment.hour)
        }
        if grouped and max([len(grouped[h]) for h in grouped]) > 1:
            self.fluctuations_lt_droite = max([
                amplitude([e.pio.droite for e in grouped[heure]])
                for heure in grouped
            ])
            self.fluctuations_lt_gauche = max([
                amplitude([e.pio.gauche for e in grouped[heure]])
                for heure in grouped
            ])
        else:
            self.fluctuations_lt_droite = None
            self.fluctuations_lt_gauche = None

        if examens:
            latest = max([e.moment for e in examens])
            current = [e for e in examens if (latest - e.moment) <= ct_limit]
            times = set([e.moment.hour for e in current])
            if len(times) > 3:
                self.fluctuations_ct_droite = amplitude(
                    [e.pio.droite for e in current])
                self.fluctuations_ct_gauche = amplitude(
                    [e.pio.gauche for e in current])
            else:
                self.fluctuations_ct_droite = None
                self.fluctuations_ct_gauche = None
        else:
            self.fluctuations_ct_droite = None
            self.fluctuations_ct_gauche = None


class PIOStatsTable(QTableWidget):
    def __init__(self, examens: List[Examen]):
        QTableWidget.__init__(self, 5, 2)
        self.setHorizontalHeaderLabels(["droite", "gauche"])
        self.setVerticalHeaderLabels(
            ["Pic", "Creux", "Moyenne", "Fluctuations LT", "Fluctuations CT"])
        self.verticalScrollBar().setEnabled(False)
        self.horizontalScrollBar().setEnabled(False)
        self.setVerticalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setHorizontalScrollBarPolicy(Qt.ScrollBarAlwaysOff)
        self.setMaximumSize(self.getQTableWidgetSize())
        self.setMinimumSize(self.getQTableWidgetSize())

        stats = PIOStats(examens)
        if stats.pic_droite is not None:
            # Tous les pics et creux sont présents
            self.setItem(0, 0, QTableWidgetItem(f"{stats.pic_droite}"))
            self.setItem(0, 1, QTableWidgetItem(f"{stats.pic_gauche}"))
            self.setItem(1, 0, QTableWidgetItem(f"{stats.creux_droite}"))
            self.setItem(1, 1, QTableWidgetItem(f"{stats.creux_gauche}"))
            self.setItem(2, 0, QTableWidgetItem(f"{stats.moyenne_droite:.2f}"))
            self.setItem(2, 1, QTableWidgetItem(f"{stats.moyenne_gauche:.2f}"))
        else:
            for x in range(3):
                for y in range(2):
                    item = QTableWidgetItem("*")
                    item.setToolTip(
                        "Pic, creux et moyennes présents seulement si une ou plusieurs "
                        + "mesures sont faites")
                    self.setItem(x, y, item)

        if stats.fluctuations_lt_droite is not None:
            # Toutes les fluctuations long terme sont présentes
            self.setItem(3, 0,
                         QTableWidgetItem(f"{stats.fluctuations_lt_droite}"))
            self.setItem(3, 1,
                         QTableWidgetItem(f"{stats.fluctuations_lt_gauche}"))
        else:
            item = QTableWidgetItem("*")
            item.setToolTip(
                "Fluctuations long terme présentes seulement si plusieurs" +
                " examens sont effectués à la même heure")
            self.setItem(3, 0, item)
            item = QTableWidgetItem("*")
            item.setToolTip(
                "Fluctuations long terme présentes seulement si plusieurs" +
                " examens sont effectués à la même heure")
            self.setItem(3, 1, item)

        if stats.fluctuations_ct_droite is not None:
            # Toutes les fluctuations court terme sont présentes
            self.setItem(4, 0,
                         QTableWidgetItem(f"{stats.fluctuations_ct_droite}"))
            self.setItem(4, 1,
                         QTableWidgetItem(f"{stats.fluctuations_ct_gauche}"))
        else:
            item = QTableWidgetItem("*")
            item.setToolTip(
                "Fluctuations court terme présentes seulement si des" +
                " examens récents sont effectués à des heures différentes")
            self.setItem(4, 0, item)
            item = QTableWidgetItem("*")
            item.setToolTip(
                "Fluctuations court terme présentes seulement si des" +
                " examens récents sont effectués à des heures différentes")
            self.setItem(4, 1, item)

    def getQTableWidgetSize(self):
        rowTotalHeight = 0
        for i in range(self.verticalHeader().count()):
            rowTotalHeight += self.verticalHeader().sectionSize(i)
        h = (self.horizontalHeader().height() + rowTotalHeight)

        colTotalWidth = 0
        for i in range(self.horizontalHeader().count()):
            colTotalWidth += self.horizontalHeader().sectionSize(i)
        w = (self.verticalHeader().width() + self.verticalScrollBar().width() +
             colTotalWidth)
        return QSize(w, h)


class PIOChart(QWidget):
    def __init__(self, traitement: Traitement, reference: Traitement):
        QWidget.__init__(self)

        examens = traitement.examens

        CHART_HOUR_MIN = 8
        CHART_HOUR_MAX = 20

        layout = QVBoxLayout()

        chart_droite = QtCharts.QChart()
        chart_droite.legend().hide()
        chart_droite.createDefaultAxes()
        chart_droite.setTitle("PIO droite")

        view_droite = QtCharts.QChartView(chart_droite)
        view_droite.setFixedHeight(250)
        view_droite.setRenderHint(QPainter.Antialiasing)
        layout.addWidget(view_droite)

        chart_fluct_droite = QtCharts.QChart()
        chart_fluct_droite.legend().hide()
        chart_fluct_droite.createDefaultAxes()
        chart_fluct_droite.setTitle("Fluctuations")

        view_fluct_droite = QtCharts.QChartView(chart_fluct_droite)
        view_fluct_droite.setFixedHeight(200)
        view_fluct_droite.setRenderHint(QPainter.Antialiasing)
        layout.addWidget(view_fluct_droite)

        chart_gauche = QtCharts.QChart()
        chart_gauche.legend().hide()
        chart_gauche.createDefaultAxes()
        chart_gauche.setTitle("PIO gauche")

        view_gauche = QtCharts.QChartView(chart_gauche)
        view_gauche.setFixedHeight(250)
        view_gauche.setRenderHint(QPainter.Antialiasing)
        layout.addWidget(view_gauche)

        chart_fluct_gauche = QtCharts.QChart()
        chart_fluct_gauche.legend().hide()
        chart_fluct_gauche.createDefaultAxes()
        chart_fluct_gauche.setTitle("Fluctuations")

        view_fluct_gauche = QtCharts.QChartView(chart_fluct_gauche)
        view_fluct_gauche.setFixedHeight(200)
        view_fluct_gauche.setRenderHint(QPainter.Antialiasing)
        layout.addWidget(view_fluct_gauche)

        self.setLayout(layout)

        if not examens:
            return

        series_droite = QtCharts.QScatterSeries()
        series_gauche = QtCharts.QScatterSeries()

        d, g = to_scatter_points(examens)
        series_droite.append(d)
        series_gauche.append(g)
        series_droite.setMarkerSize(8)
        series_gauche.setMarkerSize(8)

        chart_droite.addSeries(series_droite)

        chart_gauche.addSeries(series_gauche)

        axisx_droite = QtCharts.QValueAxis()
        axisx_droite.setRange(CHART_HOUR_MIN, CHART_HOUR_MAX)
        axisx_droite.setTickCount(5)
        axisx_droite.setLabelFormat("%dh")
        chart_droite.setAxisX(axisx_droite, series_droite)

        min_droite = min([e.pio.droite for e in examens])
        max_droite = max([e.pio.droite for e in examens])
        min_gauche = min([e.pio.gauche for e in examens])
        max_gauche = max([e.pio.gauche for e in examens])
        if reference and reference.examens:
            ref_min_droite = min([
                e.pio.droite * (1 - traitement.objectifReduction)
                for e in reference.examens
            ])
            ref_max_droite = max([
                e.pio.droite * (1 - traitement.objectifReduction)
                for e in reference.examens
            ])
            ref_min_gauche = min([
                e.pio.gauche * (1 - traitement.objectifReduction)
                for e in reference.examens
            ])
            ref_max_gauche = max([
                e.pio.gauche * (1 - traitement.objectifReduction)
                for e in reference.examens
            ])
            min_droite = min(min_droite, ref_min_droite)
            max_droite = max(max_droite, ref_max_droite)
            min_gauche = min(min_gauche, ref_min_gauche)
            max_gauche = max(max_gauche, ref_max_gauche)

        axisy_droite = QtCharts.QValueAxis()
        axisy_droite.setRange(min_droite - 1, max_droite + 1)
        chart_droite.setAxisY(axisy_droite, series_droite)

        axisx_gauche = QtCharts.QValueAxis()
        axisx_gauche.setRange(CHART_HOUR_MIN, CHART_HOUR_MAX)
        axisx_gauche.setTickCount(5)
        axisx_gauche.setLabelFormat("%dh")
        chart_gauche.setAxisX(axisx_gauche, series_gauche)

        axisy_gauche = QtCharts.QValueAxis()
        axisy_gauche.setRange(min_gauche - 1, max_gauche + 1)
        chart_gauche.setAxisY(axisy_gauche, series_gauche)

        sseries_droite = QtCharts.QSplineSeries()
        sseries_gauche = QtCharts.QSplineSeries()
        sd, sg = to_spline_points(examens)
        sseries_droite.append(sd)
        sseries_gauche.append(sg)
        chart_droite.addSeries(sseries_droite)
        chart_droite.setAxisX(axisx_droite, sseries_droite)
        chart_droite.setAxisY(axisy_droite, sseries_droite)
        chart_gauche.addSeries(sseries_gauche)
        chart_gauche.setAxisX(axisx_gauche, sseries_gauche)
        chart_gauche.setAxisY(axisy_gauche, sseries_gauche)

        if reference and reference.examens:
            rseries_droite = QtCharts.QSplineSeries()
            rseries_gauche = QtCharts.QSplineSeries()
            rd, rg = to_spline_points(reference.examens,
                                      traitement.objectifReduction)
            rseries_droite.append(rd)
            rseries_gauche.append(rg)
            chart_droite.addSeries(rseries_droite)
            chart_droite.setAxisX(axisx_droite, rseries_droite)
            chart_droite.setAxisY(axisy_droite, rseries_droite)
            chart_gauche.addSeries(rseries_gauche)
            chart_gauche.setAxisX(axisx_gauche, rseries_gauche)
            chart_gauche.setAxisY(axisy_gauche, rseries_gauche)

        axisx_fluct_droite = QtCharts.QValueAxis()
        axisx_fluct_droite.setRange(CHART_HOUR_MIN, CHART_HOUR_MAX)
        axisx_fluct_droite.setTickCount(5)
        axisx_fluct_droite.setLabelFormat("%dh")

        axisx_fluct_gauche = QtCharts.QValueAxis()
        axisx_fluct_gauche.setRange(CHART_HOUR_MIN, CHART_HOUR_MAX)
        axisx_fluct_gauche.setTickCount(5)
        axisx_fluct_gauche.setLabelFormat("%dh")

        sseries_fluct_droite = QtCharts.QSplineSeries()
        sseries_fluct_gauche = QtCharts.QSplineSeries()
        sd, sg = to_fluctuation_points(examens)

        axisy_fluct_droite = QtCharts.QValueAxis()
        axisy_fluct_droite.setRange(0, max([e.y() for e in sd]) + 1)

        axisy_fluct_gauche = QtCharts.QValueAxis()
        axisy_fluct_gauche.setRange(0, max([e.y() for e in sg]) + 1)

        sseries_fluct_droite.append(sd)
        sseries_fluct_gauche.append(sg)
        chart_fluct_droite.addSeries(sseries_fluct_droite)
        chart_fluct_droite.setAxisX(axisx_fluct_droite, sseries_fluct_droite)
        chart_fluct_droite.setAxisY(axisy_fluct_droite, sseries_fluct_droite)
        chart_fluct_gauche.addSeries(sseries_fluct_gauche)
        chart_fluct_gauche.setAxisX(axisx_fluct_gauche, sseries_fluct_gauche)
        chart_fluct_gauche.setAxisY(axisy_fluct_gauche, sseries_fluct_gauche)


class TraitementWidget(QWidget):
    def __init__(self, traitement: Traitement, reference: Traitement):
        QWidget.__init__(self)

        self.traitement = traitement
        layout = QVBoxLayout()
        ml = ",".join([str(m.name) for m in traitement.medicaments])
        layout.addWidget(
            QLabel((
                f"<h2>Traitement: {ml}</h2>"
                f"<ul>"
                f"<li>début: {traitement.debut.year}-{traitement.debut.month}"
                f"-{traitement.debut.day}</li>"
                f"<li>objectif de réduction: {int(traitement.objectifReduction * 100)}%"
                f"<li></li>"
                f"</ul>")))
        layout.addWidget(PIOChart(traitement, reference))
        layout.addWidget(PIOStatsTable(traitement.examens))
        self.setLayout(layout)


class PatientWidget(QScrollArea):
    def __init__(self, patient):
        QScrollArea.__init__(self)

        self.patient = patient
        self.update()

    def setPatient(self, patient):
        self.patient = patient
        self.update()

    def titleWidget(self):
        return QLabel(
            (f"<h1>Fiche patient</h1>"
             f"<ul>"
             f"<li>Nom: {self.patient.prenom} {self.patient.nom}</li>"
             f"<li>Date de naissance: {self.patient.naissance.year}-"
             f"{self.patient.naissance.month}-{self.patient.naissance.day}</li>"
             f"<li>Type de glaucome: {self.patient.typeGlaucome.screen_name()}</li>"
             f"<li>Notes: {self.patient.note}</li>"
             f"<li>Réfraction: {self.patient.refractionDroite:.2f} : "
             f"{self.patient.refractionGauche:.2f}</li>"
             f"<li>Pachymétrie: {self.patient.pachymetrieDroite} µm : "
             f"{self.patient.pachymetrieGauche} µm</li>"
             f"</ul>"))

    def update(self):
        layout = QVBoxLayout()
        layout.addWidget(self.titleWidget())

        reference = None
        if self.patient.traitements:
            reference = self.patient.traitements[0]

        for traitement in reversed(self.patient.traitements):
            if reference != traitement:
                layout.addWidget(TraitementWidget(traitement, reference))
            else:
                layout.addWidget(TraitementWidget(traitement, None))

        layout.addStretch()

        # scrollArea = QScrollArea()
        self.setWidgetResizable(True)

        inner = QFrame()
        inner.setLayout(layout)

        self.setWidget(inner)

        # self.setLayout(scrollArea)

    def print_(self, printer):
        painter = QPainter()
        printer.setResolution(600)
        painter.begin(printer)
        width = 800
        xscale = printer.pageRect().width() / width
        yscale = printer.pageRect().height() / self.height()
        scale = min(xscale, yscale)
        painter.scale(scale, scale)
        reference = None
        if self.patient.traitements:
            reference = self.patient.traitements[0]
        for traitement in reversed(self.patient.traitements):
            point = QPoint()
            if traitement is not self.patient.traitements[-1]:
                printer.newPage()
            else:
                tw = self.titleWidget()
                tw.setStyleSheet("background-color: white")
                tw.render(painter, QPoint())
                point = QPoint(0, tw.height())

            widg = TraitementWidget(
                traitement, reference if reference != traitement else None)
            widg.setStyleSheet("background-color: white")
            widg.resize(width, widg.height())
            widg.render(painter, point)


class WelcomeTab(QLabel):
    def __init__(self):
        QLabel.__init__(
            self, """ <h1>Application de suivi de pression intra-oculaire</h1>
        <p></p> <p>Pour ouvrir une fiche patient, sélectionnez
        "Patient | Ouvrir fichier patient..." ou faites
        "Ctrl+O"</p>
        <p>Pour créer une nouvelle fiche patient, selectionnez
        "Patient | Nouveau patient...", ou faites "Ctrl+N"</p> """)
        self.setAlignment(Qt.AlignTop | Qt.AlignLeft)


class dialogNouveauTraitement(QDialog):
    def __init__(self, parent):
        QDialog.__init__(self, parent)
        layout = QFormLayout()

        self.lesMedicaments = QListWidget()
        for m in Medicament:
            item = QListWidgetItem(f"{m.name}", self.lesMedicaments)
            item.setFlags(item.flags() | Qt.ItemIsUserCheckable)
            item.setCheckState(Qt.Unchecked)
            item.setData(Qt.UserRole, m)
        layout.addRow(self.lesMedicaments)

        self.leDebut = QCalendarWidget()
        layout.addRow("Date de début", self.leDebut)

        self.lObjectifReduction = QSpinBox()
        self.lObjectifReduction.setRange(0, 100)
        layout.addRow("Objectif de réduction (%)", self.lObjectifReduction)

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok
                                     | QDialogButtonBox.Cancel)

        buttonBox.accepted.connect(lambda: self.accept())
        buttonBox.rejected.connect(lambda: self.reject())

        layout.addRow(buttonBox)

        self.setLayout(layout)
        self.setWindowTitle("Nouveau traitement")
        self.setModal(True)

    def traitement(self):
        medicaments = []
        for r in range(self.lesMedicaments.count()):
            if self.lesMedicaments.item(r).checkState() == Qt.Checked:
                medicaments.append(
                    self.lesMedicaments.item(r).data(Qt.UserRole))

        return Traitement(
            medicaments,
            dt.datetime.combine(self.leDebut.selectedDate().toPython(),
                                dt.time()),
            self.lObjectifReduction.value() / 100, [])


def mots_correspondent(patient: Patient, mots: List[str]) -> bool:
    """Vrai ssi les mots recherchés sont inclus dans le nom et/ou le prénom du
    patient
    """
    for mot in mots:
        if (mot.lower() not in patient.prenom.lower()
                and mot.lower() not in patient.nom.lower()):
            return False
    return True


class dialogOuvrirPatient(QDialog):
    def __init__(self, parent, database: List[Patient]):
        QDialog.__init__(self, parent)

        filterLineEdit = QLineEdit()
        filterLineEdit.textChanged.connect(
            lambda text: self.updateFilter(text))

        self.listWidget = QListWidget()
        for patient in database:
            item = QListWidgetItem(f"{patient.nom}, {patient.prenom}")
            item.setData(Qt.UserRole, patient)
            self.listWidget.addItem(item)

        layout = QVBoxLayout()
        layout.addWidget(filterLineEdit)
        layout.addWidget(self.listWidget)

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok
                                     | QDialogButtonBox.Cancel)

        buttonBox.accepted.connect(lambda: self.verify())
        buttonBox.rejected.connect(lambda: self.reject())

        layout.addWidget(buttonBox)
        self.setLayout(layout)
        self.setWindowTitle("Ouvrir patient")
        self.setModal(True)

    def patient(self):
        return self.listWidget.selectedItems()[0].data(Qt.UserRole)

    def verify(self):
        if len(self.listWidget.selectedItems()) == 1:
            self.accept()
            return
        QMessageBox.warning(self, "Patient non sélectionné",
                            "Un patient doit être sélectionné")

    def updateFilter(self, text: str):
        mots = text.split(" ")
        for i in range(self.listWidget.count()):
            item = self.listWidget.item(i)
            patient = item.data(Qt.UserRole)
            item.setHidden(
                False if mots_correspondent(patient, mots) else True)


class dialogNouveauPatient(QDialog):
    def __init__(self, parent):
        QDialog.__init__(self, parent)
        layout = QFormLayout()

        self.lePrenom = QLineEdit()
        layout.addRow("Prénom", self.lePrenom)

        self.leNom = QLineEdit()
        layout.addRow("Nom", self.leNom)

        self.laDateDeNaissance = QCalendarWidget()
        layout.addRow("Date de naissance", self.laDateDeNaissance)

        self.typesGlaucome = {}
        buttonBox = QGroupBox()
        layout.addRow("Type de glaucome", buttonBox)
        buttonLayout = QVBoxLayout()
        buttonBox.setLayout(buttonLayout)
        for typeglauc in TypeGlaucome:
            but = QRadioButton(typeglauc.screen_name())
            buttonLayout.addWidget(but)
            self.typesGlaucome[typeglauc] = but

        self.laNote = QTextEdit()
        layout.addRow("Notes", self.laNote)

        self.laRefractionDroite = QDoubleSpinBox()
        self.laRefractionDroite.setRange(-99, 99)
        layout.addRow("Réfraction droite", self.laRefractionDroite)

        self.laRefractionGauche = QDoubleSpinBox()
        self.laRefractionGauche.setRange(-99, 99)
        layout.addRow("Réfraction gauche", self.laRefractionGauche)

        self.laPachymetrieDroite = QSpinBox()
        self.laPachymetrieDroite.setRange(0, 999)
        layout.addRow("Pachymétrie droite", self.laPachymetrieDroite)

        self.laPachymetrieGauche = QSpinBox()
        self.laPachymetrieGauche.setRange(0, 999)
        layout.addRow("Pachymétrie gauche", self.laPachymetrieGauche)

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok
                                     | QDialogButtonBox.Cancel)

        buttonBox.accepted.connect(lambda: self.verify())
        buttonBox.rejected.connect(lambda: self.reject())

        layout.addRow(buttonBox)

        self.setLayout(layout)
        self.setWindowTitle("Nouveau patient")
        self.setModal(True)

    def lireTypeGlaucome(self):
        for typeglauc in self.typesGlaucome:
            if self.typesGlaucome[typeglauc].isChecked():
                return typeglauc
        return None

    def patient(self):
        return Patient(
            self.lePrenom.text(),
            self.leNom.text(),
            dt.datetime.combine(
                self.laDateDeNaissance.selectedDate().toPython(), dt.time()),
            self.lireTypeGlaucome(),
            self.laNote.toPlainText(), self.laRefractionDroite.value(),
            self.laRefractionGauche.value(), self.laPachymetrieDroite.value(),
            self.laPachymetrieGauche.value(), [])

    def verify(self):
        if not self.lePrenom.text():
            QMessageBox.warning(self, "Formulaire incomplet",
                                "Le prénom est vide. Merci de le remplir")
            return

        if not self.leNom.text():
            QMessageBox.warning(self, "Formulaire incomplet",
                                "Le nom est vide. Merci de le remplir")
            return

        if self.lireTypeGlaucome() is None:
            QMessageBox.warning(
                self, "Formulaire incomplet",
                "Le type de glaucome n'est pas sélectionné. Merci de le remplir"
            )
            return

        self.accept()


class dialogNouvelExamen(QDialog):
    def __init__(self, parent):
        QDialog.__init__(self, parent)
        layout = QFormLayout()

        self.laDate = QCalendarWidget()
        layout.addRow("Date", self.laDate)

        self.lHeure = QTimeEdit()
        self.lHeure.setTime(QTime.currentTime())
        layout.addRow("Heure", self.lHeure)

        self.laPIODroite = QDoubleSpinBox()
        layout.addRow("PIO droite", self.laPIODroite)

        self.laPIOGauche = QDoubleSpinBox()
        layout.addRow("PIO gauche", self.laPIOGauche)

        buttonBox = QDialogButtonBox(QDialogButtonBox.Ok
                                     | QDialogButtonBox.Cancel)

        buttonBox.accepted.connect(lambda: self.accept())
        buttonBox.rejected.connect(lambda: self.reject())

        layout.addRow(buttonBox)

        self.setLayout(layout)
        self.setWindowTitle("Nouvel Examen")
        self.setModal(True)

    def examen(self):
        return Examen(
            dt.datetime.combine(self.laDate.selectedDate().toPython(),
                                self.lHeure.time().toPython()),
            PIO(self.laPIODroite.value(), self.laPIOGauche.value()))


def contains_patient(database: List[Patient], needle: Patient) -> bool:
    for patient in database:
        if all([
                patient.prenom == needle.prenom,
                patient.nom == needle.nom,
                patient.naissance == needle.naissance,
        ]):
            return True
    return False


def merge_databases(original: List[Patient],
                    updated: List[Patient]) -> List[Patient]:
    merged = updated
    for patient in original:
        if not contains_patient(merged, patient):
            merged.append(patient)
    return merged


class MainWindow(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        self.setWindowTitle("Application de suivi de pression intra-oculaire")
        self.tabWidget = QTabWidget()
        self.setCentralWidget(self.tabWidget)

        self.tabWidget.setTabsClosable(True)
        self.tabWidget.tabCloseRequested.connect(
            lambda idx: self.closePatient(idx))

        self.accueil()

        database = load_database_file()

        menuBar = QMenuBar()
        self.setMenuBar(menuBar)
        # menuBar = self.menuBar()

        menuBaseDonnees = menuBar.addMenu("Base de données")
        actionAccueil = menuBaseDonnees.addAction("Accueil")
        actionAccueil.triggered.connect(lambda: self.accueil())
        actionAccueil.setShortcut(QKeySequence("Ctrl+A"))

        actOuvrir = menuBaseDonnees.addAction("Ouvrir")
        actOuvrir.triggered.connect(lambda: self.ouvrirPatient())
        actOuvrir.setShortcut(QKeySequence("Ctrl+O"))

        actSauvegarder = menuBaseDonnees.addAction("Sauvegarder")
        actSauvegarder.triggered.connect(lambda: self.sauverBaseDonnees())
        actSauvegarder.setShortcut(QKeySequence("Ctrl+S"))

        actNouveauPatient = menuBaseDonnees.addAction("Nouveau patient...")
        actNouveauPatient.triggered.connect(lambda: self.nouveauPatient())
        actNouveauPatient.setShortcut(QKeySequence("Ctrl+N"))

        actImporterPatient = menuBaseDonnees.addAction("Importer patient...")
        actImporterPatient.triggered.connect(lambda: self.importerPatient())
        actImporterPatient.setShortcut(QKeySequence("Ctrl+I"))

        actExporterPatient = menuBaseDonnees.addAction("Exporter patient...")
        actExporterPatient.triggered.connect(lambda: self.exporterPatient())
        actExporterPatient.setShortcut(QKeySequence("Ctrl+R"))

        closeAction = menuBaseDonnees.addAction("Fermer fiche courante")
        closeAction.triggered.connect(lambda: self.closeCurrentPatient())
        closeAction.setShortcut(QKeySequence("Ctrl+W"))

        menuPatient = menuBar.addMenu("Patient")
        nvTraitement = menuPatient.addAction("Nouveau Traitement...")
        nvTraitement.triggered.connect(lambda: self.nouveauTraitement())
        nvTraitement.setShortcut(QKeySequence("Ctrl+T"))

        nvExamen = menuPatient.addAction("Nouvel examen...")
        nvExamen.triggered.connect(lambda: self.nouvelExamen())
        nvExamen.setShortcut(QKeySequence("Ctrl+E"))

        actImprimer = menuPatient.addAction("Imprimer...")
        actImprimer.triggered.connect(lambda: self.impression())
        actImprimer.setShortcut(QKeySequence("Ctrl+P"))

    def accueil(self):
        self.tabWidget.addTab(WelcomeTab(), "Accueil")
        self.tabWidget.setCurrentIndex(self.tabWidget.count() - 1)

    def currentPatients(self):
        current = []
        for i in range(self.tabWidget.count()):
            try:
                patient = self.tabWidget.widget(i).patient
                if contains_patient(current, patient):
                    print("Double patient open. Ignoring one.")
                else:
                    current.append(patient)
            except AttributeError:
                pass
        return current

    def sauverBaseDonnees(self):
        database = merge_databases(load_database_file(),
                                   self.currentPatients())
        save_database_file(database)

    def ouvrirPatient(self):
        dlg = dialogOuvrirPatient(self, load_database_file())
        dlg.exec()
        if dlg.result() == QDialog.Accepted:
            patient = dlg.patient()
            self.tabWidget.addTab(
                PatientWidget(patient), f"{patient.prenom} {patient.nom}")
            self.tabWidget.setCurrentIndex(self.tabWidget.count() - 1)

    def nouveauTraitement(self):
        try:
            patient = self.tabWidget.currentWidget().patient
            dlg = dialogNouveauTraitement(self)
            dlg.exec()
            if dlg.result() == QDialog.Accepted:
                traitement = dlg.traitement()
                patient.traitements.append(traitement)
                self.tabWidget.currentWidget().setPatient(patient)
        except AttributeError:
            QMessageBox.critical(
                self, "Création de traitement impossible",
                ("Il n'est possible de créer un traitement que si un patient"
                 " est sélectionné"))

    def impression(self):
        dialog = QPrintDialog(self)
        if dialog.exec() == QDialog.Accepted:
            self.tabWidget.currentWidget().print_(dialog.printer())

    def nouvelExamen(self):
        try:
            patient_courant = self.tabWidget.currentWidget().patient
            examens_courants = patient_courant.traitements[-1].examens
            dlg = dialogNouvelExamen(self)
            dlg.exec()
            if dlg.result() == QDialog.Accepted:
                examens_courants.append(dlg.examen())
                self.tabWidget.currentWidget().setPatient(patient_courant)
        except (AttributeError, IndexError):
            QMessageBox.critical(
                self, "Création d'examen impossible",
                ("Il n'est possible de créer un examen que si un patient avec"
                 " au moins un traitement est sélectionné. Ne pas sélectionner"
                 " de traitement spécifique si création d'une courbe de "
                 "référence sans traitement"))

    def importerPatient(self):
        try:
            fileName = QFileDialog.getOpenFileName(self, "Importer patient...",
                                                   ".", "Fichiers MGC (*.mgc)")
            patient = load_patient_file(fileName[0])
            self.tabWidget.addTab(
                PatientWidget(patient), f"{patient.prenom} {patient.nom}")
            self.tabWidget.setCurrentIndex(self.tabWidget.count() - 1)
        except Exception as e:
            print(f"error: {e}")

    def exporterPatient(self):
        try:
            fileName = QFileDialog.getSaveFileName(self, "Exporter patient",
                                                   ".", "Fichiers MGC (*.mgc)")
            save_patient_file(self.tabWidget.currentWidget().patient,
                              fileName[0])
            QMessageBox.information(self, "Export effectuée",
                                    "Données patient exportées")
        except Exception as e:
            print(f"error: {e}")

    def closeCurrentPatient(self):
        self.closePatient(self.tabWidget.currentIndex())

    def closePatient(self, index: int):
        bd_fichier = sorted(load_database_file())
        current_db = sorted(
            merge_databases(load_database_file(), self.currentPatients()))
        if bd_fichier != current_db:
            confirm = QMessageBox.warning(
                self, "Changements non sauvegardés",
                ("Certaines données ne sont pas sauvegardées. Des changements"
                 " pourraient être perdus. Voulez vous continuer?"),
                QMessageBox.Yes | QMessageBox.No)
            if confirm == QMessageBox.No:
                return
        self.tabWidget.removeTab(index)

    def nouveauPatient(self):
        dlg = dialogNouveauPatient(self)
        dlg.exec()
        if dlg.result() == QDialog.Accepted:
            patient = dlg.patient()
            self.tabWidget.addTab(
                PatientWidget(patient), f"{patient.prenom} {patient.nom}")
            self.tabWidget.setCurrentIndex(self.tabWidget.count() - 1)

    def closeEvent(self, event):
        bd_fichier = sorted(load_database_file())
        current_db = sorted(
            merge_databases(load_database_file(), self.currentPatients()))
        if bd_fichier != current_db:
            confirm = QMessageBox.warning(
                self, "Changements non sauvegardés",
                ("Certaines données ne sont pas sauvegardées. Des changements"
                 " pourraient être perdus. Voulez vous continuer?"),
                QMessageBox.Yes | QMessageBox.No)
            if confirm == QMessageBox.No:
                event.ignore()
                return
        QMainWindow.closeEvent(self, event)


if __name__ == "__main__":
    app = QApplication(sys.argv)

    mw = MainWindow()
    mw.resize(800, 600)
    mw.show()

    sys.exit(app.exec_())
