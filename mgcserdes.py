from datetime import datetime
from typing import List
import json
import os

from marshmallow import Schema, fields, pprint, post_load
from marshmallow_enum import EnumField

from mgcdata import (
    PIO,
    Examen,
    Medicament,
    Traitement,
    TypeGlaucome,
    Patient,
)


class PIOSchema(Schema):
    droite = fields.Number()
    gauche = fields.Number()

    @post_load
    def make_pio(self, data):
        return PIO(**data)


class ExamenSchema(Schema):
    moment = fields.DateTime()
    pio = fields.Nested(PIOSchema)

    @post_load
    def make_examen(self, data):
        return Examen(**data)


class TraitementSchema(Schema):
    medicaments = fields.List(EnumField(Medicament))
    debut = fields.DateTime()
    objectifReduction = fields.Number()
    examens = fields.List(fields.Nested(ExamenSchema))

    @post_load
    def make_traitement(self, data):
        return Traitement(**data)


class PatientSchema(Schema):
    prenom = fields.Str()
    nom = fields.Str()
    naissance = fields.DateTime()
    typeGlaucome = EnumField(TypeGlaucome)
    note = fields.Str()
    refractionDroite = fields.Number()
    refractionGauche = fields.Number()
    pachymetrieDroite = fields.Number()
    pachymetrieGauche = fields.Number()
    traitements = fields.List(fields.Nested(TraitementSchema))

    @post_load
    def make_patient(self, data):
        return Patient(**data)


def save_patient_file(patient, filename):
    try:
        with open(filename, 'w') as f:
            schema = PatientSchema(strict=True)
            result = schema.dump(patient)
            f.write(json.dumps(result.data, indent=2))
    except IOError:
        print(f"error: could not open file {filename} for writing")


def load_patient_file(filename):
    try:
        with open(filename, 'r') as f:
            patient_data = json.load(f)
            schema = PatientSchema(strict=True)
            result = schema.load(patient_data)
            return result.data
    except IOError:
        print(f"error: could not open file {filename} for reading")


DATABASE_PATH = os.path.expanduser("~/mgc/database.mgcdb")


def load_database_file() -> List[Patient]:
    try:
        with open(DATABASE_PATH, 'r') as f:
            patient_data = json.load(f)
            schema = PatientSchema(strict=True, many=True)
            result = schema.load(patient_data)
            return result.data
    except IOError:
        print(f"{DATABASE_PATH} not found")
        return []


def save_database_file(patients: List[Patient]):
    os.makedirs(os.path.dirname(DATABASE_PATH), exist_ok=True)
    try:
        with open(DATABASE_PATH, 'w') as f:
            schema = PatientSchema(strict=True, many=True)
            result = schema.dump(patients)
            f.write(json.dumps(result.data, indent=2))
    except IOError:
        print(f"error: could not open file {DATABASE_PATH} for writing")


if __name__ == "__main__":
    traitements = [
        Traitement([Medicament.Combigan], datetime(2018, 1, 1), 0.1, [
            Examen(datetime(2017, 1, 2, 9), PIO(9, 10)),
            Examen(datetime(2017, 1, 3, 10), PIO(11, 10)),
            Examen(datetime(2017, 1, 4, 11), PIO(13, 11)),
            Examen(datetime(2017, 1, 5, 12), PIO(15, 12)),
            Examen(datetime(2017, 1, 5, 12), PIO(16, 14)),
            Examen(datetime(2017, 1, 5, 12), PIO(14, 11)),
            Examen(datetime(2017, 1, 6, 13), PIO(16, 13)),
            Examen(datetime(2017, 1, 7, 14), PIO(14, 14)),
        ]),
        Traitement([Medicament.Cartéol], datetime(2018, 1, 1), 0.1, [
            Examen(datetime(2017, 1, 2, 9), PIO(9, 20)),
            Examen(datetime(2017, 1, 3, 10), PIO(11, 10)),
            Examen(datetime(2017, 1, 4, 11), PIO(13, 11)),
            Examen(datetime(2017, 1, 5, 12), PIO(15, 12)),
            Examen(datetime(2017, 1, 5, 12), PIO(16, 14)),
            Examen(datetime(2017, 1, 5, 12), PIO(14, 21)),
            Examen(datetime(2017, 1, 6, 13), PIO(16, 13)),
            Examen(datetime(2017, 1, 7, 14), PIO(14, 24)),
        ]),
    ]

    patient = Patient(
        "Jean-Eude",
        "Bocassin",
        datetime(1920, 11, 20),
        traitements,
    )
    schema = PatientSchema(strict=True)
    result = schema.dump(patient)

    print(result.data)

    save_patient_file(patient, 'bocassin.mgc')

    pprint(load_patient_file('bocassin.mgc'))
