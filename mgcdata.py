from typing import (
    List,
    NamedTuple,
)

from enum import Enum

from datetime import datetime


class PIO(NamedTuple):
    droite: float
    gauche: float


class Examen(NamedTuple):
    moment: datetime
    pio: PIO


class Medicament(Enum):
    Alphagan = 1
    Azarga = 2
    Azopt = 3
    Betagan = 4
    Betoptic = 5
    Cartéol = 6
    Cartéol_LP = 7
    Combigan = 8
    Cosidime = 9
    Diamox = 10
    Dualkopt = 11
    Duotrav = 12
    Ganfort = 13
    Geltim = 14
    Lumigan = 15
    Monoprost = 16
    Nyogel_LP = 17
    Ophtim = 18
    Simbrinza = 19
    Timabak = 20
    Timocomod = 21
    Timoptol_LP = 22
    Travatan = 23
    Trusopt = 24
    Xalacom = 25
    Xalatan = 26
    SLT = 27
    Trabeculectomie = 28
    SNP = 29
    PKE = 30


class Traitement(NamedTuple):
    medicaments: List[Medicament]
    debut: datetime
    objectifReduction: float
    examens: List[Examen]


class TypeGlaucome(Enum):
    OHT = 1
    SXF = 2
    GAO = 3
    GPN = 4
    GXF = 5
    GPT = 6
    GFA = 7
    GJN = 8
    GCG = 9
    Autre = 10

    def screen_name(self):
        nameDict = {
            TypeGlaucome.OHT: "Hypertonie oculaire",
            TypeGlaucome.SXF: "Syndrome exfoliatif",
            TypeGlaucome.GAO: "Glaucome primitif à Angle Ouvert",
            TypeGlaucome.GPN: "Glaucome à pression normale",
            TypeGlaucome.GXF: "Glaucome Exfoliatif",
            TypeGlaucome.GPT: "Glaucome Pigmentaire",
            TypeGlaucome.GFA: "Glaucome primitif à Angle Fermé",
            TypeGlaucome.GJN: "Glaucome juvénile",
            TypeGlaucome.GCG: "Glaucome congenital",
            TypeGlaucome.Autre: "Autre",
        }
        return nameDict[self]


class Patient(NamedTuple):
    prenom: str
    nom: str
    naissance: datetime
    typeGlaucome: TypeGlaucome
    note: str
    refractionDroite: float
    refractionGauche: float
    pachymetrieDroite: int
    pachymetrieGauche: int
    traitements: List[Traitement]
