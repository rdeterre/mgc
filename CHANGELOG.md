# Journal de bord de l'application de suivi de pression intra-oculaire

Les changements notables à l'application sont listés ici.

## [1.3.0] - 2019-02-17
### Ajouts
- Graphs de fluctuations long terme

## [1.2.2] - 2019-02-02
### Correctifs
- Élément de compilation

## [1.2.1] - 2019-02-02
### Correctifs
- Indication pachymétrie en um au lieu de mm

## [1.2.0] - 2019-01-27
### Ajouts
- Nouveau médicaments (#14)
- PIO Moyenne (#17)
- Données patient supplémentaires (#16)

### Changements
- Change l'amplitude horaire affichée de 8h à 20h (#14)


## [1.1.0] - 2019-01-24
### Correctifs
- Fluctuations long terme manquantes dans certains cas (#10)
- Alerte de perte de données non affichée si les fiches patients sont fermées
  avec le bouton "x" (#12)

### Ajouts
- Support pour impression de fiche patient (#11)

### Changements
- Clarifie message d'alerte pour création d'examen sans traitement (#13)

## [1.0.0] - 2019-01-20
### Correctifs
- Affichage de courbes parfois problématique (#6)

### Changements
- Change l'amplitude horaire affichée de 7h à 19h (#9)
- Affiche courbe de référence et courbe mesurée dans leur intégralité (#7)

### Ajouts
- Base de données patients (#5)
- Alertes pour créations impossibles: nouveau traitement sans patient et nouvel
  examen sans traitement (#2)
- Signale si des changements non-sauvegardés sont présents quand on ferme une
  fenêtre patient ou l'application (#4)

## [0.1.0] - 2019-01-15
### Ajouts
- Version initiale
